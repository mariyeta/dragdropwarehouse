$(document).ready(function() {

// Initial css
	// Size and margins of the droppable area
	var drop = $("#droppable");	
	var marginDrop = Math.round(0.01*$(window).width());
	drop.css({
		width: Math.floor($(window).width() - 4 * marginDrop) + "px",
		height: Math.floor($(window).height() * 0.7) + "px",
		margin: marginDrop + "px"
	});
	var dropWidth = parseInt(drop.css("width"));
	var dropHeight = parseInt(drop.css("height"));
	// Reference to elements
	var panel = $(".panel");
	var wrapper = $(".wrapper");
	var draggable = $(".draggable");
	var rotatable = $(".rotatable");
	var wrapper_shelf = $(".wrapper_shelf");
	var draggable_shelf = $(".draggable_shelf");
	var rotatable_shelf = $(".rotatable_shelf");
	var wrapper_wall = $(".wrapper_wall");
	var draggable_wall = $(".draggable_wall");
	var rotatable_wall = $(".rotatable_wall");
	var rotate_shelf = $(".rotate_shelf");
	var rotate_wall = $(".rotate_wall");
	var remove_shelf = $(".remove_shelf");
	var remove_wall = $(".remove_wall");
	var roomform = $(".roomform");
	var footer = $(".footer");
	var cross = $("#cross");
	var tick = $("#tick");
	// Size of borders of rotatable element and droppable area
	var borderElmnt = parseInt(rotatable.css("border").split("px"));
	var borderDrop = parseInt(drop.css("border").split("px"));
	// Set width and height of draggable elements to 10% of window height (account for border size)
	var h = Math.round(0.1*$(window).height());
	panel.add(wrapper).add(draggable).css("width", h + 2 * borderElmnt + "px");
	rotatable.css("width", h + "px");
	wrapper_shelf.add(draggable_shelf).css("height", h + 2 * borderElmnt + "px");
	rotatable_shelf.css("height", h + "px");
	wrapper_wall.add(draggable_wall).css("height", Math.round(0.2 * h) + 2 * borderElmnt + "px");
	rotatable_wall.css("height", Math.round(0.2 * h) + "px");
	// Set size and position of rotate and remove images
	rotate_shelf.add(rotate_wall).add(remove_shelf).add(remove_wall).css("width", 20 + "%");
	rotate_shelf.add(remove_shelf).css("height", 20 + "%");
	rotate_wall.add(remove_wall).css("height", 100 + "%");
	rotate_shelf.css("left", 80 + "%");
	rotate_wall.css("left", 40 + "%");
	remove_shelf.css("top", 60 + "%");
	remove_wall.css("top", 0);
	// Set margins
	panel.css({
		"margin-left": marginDrop + "px",
		"margin-top": marginDrop + "px"
	});
	// Set font size
	$("html, body").css("font-size", Math.floor(0.15 * h));  


	// Initialization function
	function initClone(elmnt, counter_shelf, counter_wall) {
		var initArray = [];
		// Add "_shelf" or "_wall" according to picked element
		// Iterate approriate counter; store both counters
		initArray['counter_wall'] = counter_wall;
		initArray['counter_shelf'] = counter_shelf;
		if (elmnt.hasClass("wrapper_wall")){
			str = "_wall";
			counter = counter_wall;
			initArray['counter_wall'] = counter_wall + 1;
		}
		else {
			str = "_shelf";
			counter = counter_shelf;
			initArray['counter_shelf'] = counter_shelf + 1;
		}
		// Add classes and id to every sub div
		elmnt.attr("id","draggable"+str+"-"+counter).addClass("draggable").addClass("draggable"+str);
		elmnt.removeClass("wrapper wrapper"+str+" ui-draggable ui-draggable-dragging ui-draggable-handle");
		elmnt.find('.rotatable').attr("id","rotatable"+str+"-"+counter);
		elmnt.find('.rotate_icon').attr("id","rotate_icon"+str+"-"+counter);
		elmnt.find('.remove_icon').attr("id","remove_icon"+str+"-"+counter);
		// Add div to each corner of rotatable element (tl: top left; tr: top right; bl: bottom left; br: bottom right)
		// Corner elements will be used for determing top and left positions of all corners (usefull when element is rotated)
		initArray['rot'] = $("#rotatable"+str+"-"+counter);
		var tl = $("<div class='tl' id='tl"+str+"-"+counter+"'></div>");
		var tr = $("<div class='tr' id='tr"+str+"-"+counter+"'></div>");
		var br = $("<div class='br' id='br"+str+"-"+counter+"'></div>");
		var bl = $("<div class='bl' id='bl"+str+"-"+counter+"'></div>");
		initArray['rot'].append(tl);
		initArray['rot'].append(tr);
		initArray['rot'].append(br);
		initArray['rot'].append(bl);
		// store references to divs with rotate and remove icons		
		initArray['rotIcon'] = $("#rotate_icon"+str+"-"+counter);
		initArray['remIcon'] = $("#remove_icon"+str+"-"+counter);
		//Input text box is added for shelf element		
		if (elmnt.hasClass("draggable"+str)){
			var title = $("<input>", {
				class: "text_input",
				type: "text",
				name: "title_shelf-"+counter,
				value: "Shelf "+counter,
				maxlength: 6, 
				size: 6
			});
			elmnt.find('.text').attr("id","text"+str+"-"+counter).append(title);
			title.css("font-size", Math.floor(0.3 * h));
		}
		// elementSegments calculates all segments to which element belongs
		elementSegments(elmnt, segments, elmntBelongs);		
		return initArray;
	}

	// dividedSegments divides droppable area on 16 segments 
	// segments is an array which stores starting and ending top and left positions for each of 16 segments: s[i] = [tLow, tHight, lLow, lHigh];
	var segments = dividedSegments();
	function dividedSegments() {
		var k = 0;
		var width = drop.width();
		var height = drop.height();
		// stepWidth and stepHeight are width and height of segments
		var stepWidth = Math.ceil(width/4);
		var stepHeight = Math.ceil(height/4);
		var segments = [];	
		// tLow, lLow, tHigh, lHigh are starting and ending top and left positions of segment
		for(tLow = 1; tLow < height; tLow = tLow + stepHeight + 1) {
			for(lLow = 1; lLow < width; lLow = lLow + stepWidth + 1) {
				// If bottom and/or right positions of element exceeds droppable area, set them to height and width of droppable area
				// If dimensions are not divided by 4 evenly, last segments in that row/column will have smaller width/height than previous three
				var tHigh = ((tLow + stepHeight) > height) ? height : tLow + stepHeight;
				var lHigh = ((lLow + stepWidth) > width) ? width : lLow + stepWidth;
				segments[k] = [tLow, tHigh, lLow, lHigh];
				k++
			}
		}
		return segments;
	}
	
	// Set array of length 16; each element of array is an empty string
	var elmntBelongs = new Array(segments.length);
	for(i = 0; i < segments.length; i++) {
		elmntBelongs[i] = "";
	}


	// elementSegments calculates all segments to which element belongs
	// it returns array of length 16 containing ids of all elements, separated by coma, belonging to particular segment
	// for example, elmntBelongs[12] will be equal to "draggable_shelf-1,draggable_wall-1,draggable_shelf-2,"
	function elementSegments(elmnt, segments, elmntBelongs) {
		var dataAttr = "";
		// elmntRange represents starting and ending top and left positions of element
		var elmntRange = [elmnt.position().top, elmnt.position().top + elmnt.height(), elmnt.position().left, elmnt.position().left + elmnt.width()];
		// Check each segment to see if the element belongs to it
		for(i=0; i < segments.length; i++) {
			// Element belongs to segment if two conditions are met:
			// 1: max(element tLow, segment tLow) <= min(element tHigh, segment tHigh)
			// 2: max(element lLow, segment lLow) <= min(element lHigh, segment lHigh)
			if((Math.max(elmntRange[0], segments[i][0]) <= Math.min(elmntRange[1], segments[i][1])) && (Math.max(elmntRange[2], segments[i][2]) <= Math.min(elmntRange[3], segments[i][3]))) {
				// If id of element is not already stored for that segment, add it and add coma after it
				// elmntBelongs is an array of length 16;
				if(elmntBelongs[i].indexOf(elmnt.attr("id")) == -1) {
					elmntBelongs[i] = elmntBelongs[i] + elmnt.attr("id") + ",";		
				}
				// dataAttr is a string containg numbers of each segments that contain element - separated by coma (for example: dataAttr="0,1,4,5,")
				dataAttr = dataAttr + i + ",";
			}
			else {
				//if element does not belong to segment i, remove its id and belonging coma from elmntBelongs[i] 
				var str1 = elmntBelongs[i].replace(elmnt.attr("id") + ",", "");
				elmntBelongs[i] = str1;
			}
		}
		// delete last coma from dataAttr and add new atribute to element
		var data = dataAttr.substring(0, dataAttr.length - 1);
		elmnt.attr("data-segments", data);
		return elmntBelongs;
	}


	// Resizable room (droppable area)
	drop.resizable({ 
			// Set min and max width and height when the room is empty
			minWidth: 2 * wrapper_shelf.width() + 10,
			minHeight: 2 * wrapper_shelf.height() + 10,
			maxWidth: dropWidth,
			maxHeight: dropHeight,
			start: function(event,ui) {		
				// If there are elements in the droppable area, set new minW and minH so that room can shrink only 10px near the most left and the most bottom element
				var elmnts = $(".draggable"); 
				if(elmnts.length) {		
					var minW = 2 * wrapper_shelf.width();
					var minH = 2 * wrapper_shelf.height();
					// For each element, find the min width that area could take so that element is always inside (includeElmntW)
					// If it's larger than the current width, update
					// Do the same for height
					for (k = 0; k < elmnts.length; k++) {
						var includeElmntW = $(elmnts[k]).offset().left + $(elmnts[k]).width() - drop.offset().left;
						if(minW < includeElmntW) {
							minW = includeElmntW;
						}
						var includeElmntH = $(elmnts[k]).offset().top + $(elmnts[k]).height() - drop.offset().top;
						if(minH < includeElmntH) {
							minH = includeElmntH;
						}
					}				
					minW = minW + 10;
					minH = minH + 10;
					// Set new values of min width and height
					drop.resizable( "option", "minWidth", minW);
					drop.resizable( "option", "minHeight", minH);
				}
				drop.css("margin", marginDrop + "px");
			}
    });	

	//Remove
	//elmnt - element to be removed
	function removeElement(elmnt) {	
		var obj = "shelf";
		if (elmnt.hasClass("draggable_wall")) {
				obj = "wall";
		}
		// Ask user to confirm 
		deleting = confirm("Are you sure you want to delete this "+obj+"?");
		// Remove element and remove its id from elmntBelongs array
		if(deleting) {
			elmnt.remove();
			removeFromSegments(elmnt, segments, elmntBelongs);
		}
	}
	
	//When new clone is created, if collision occurs remove new element and reset counter  
	function removeCollision(elmnt, elmntBelongs) {						
		var id = elmnt.attr("id");
			if(detColl(elmnt)) {
				elmnt.remove();
				removeFromSegments(elmnt, segments, elmntBelongs);
				return true;
			}		
		return false;
	}

	// If element has been deleted from the screen, remove its id from elmntBelongs array
	function removeFromSegments(elmnt, segments, elmntBelongs) {
		var dataAttr = "";
		var elmntRange = [elmnt.position().top, elmnt.position().top + elmnt.height(), elmnt.position().left, elmnt.position().left + elmnt.width()];
		for(i=0; i < segments.length; i++) {
				var str = replaceString(elmntBelongs[i], elmnt.attr("id") + ",", "");
				elmntBelongs[i] = str;	
		}
		return elmntBelongs;
	}
	
	function replaceString(string, replace, newStr) {
		//find global match
		var reg = new RegExp(replace,"g");
		var str = string.replace(reg, newStr);
		string = str;
		return string
	}

	//When new clone is created, if collision occurs remove new element and reset counter  
	//Separating axis theorem is used for detecting collision
	function detColl(elmnt) {						
		var id = elmnt.attr("id");
		var tl = elmnt.find(".tl");
		var tr = elmnt.find(".tr");
		var br = elmnt.find(".br");
		var bl = elmnt.find(".bl");
		
		//variable "o" containts ids of all elements (including target element) that are inside all segments that target element belongs to
		//variable "unique" contains only unique of "o" elements (target element is removed from this variable)
		//"others" is array with ids of all elements that are isnide all segmenets that target element also covers
		var data = elmnt.attr("data-segments");			
		var eQuad = data.split(",");
		var o = "";
		for (i = 0; i < eQuad.length; i++) {
			o = o + elmntBelongs[eQuad[i]];
		}
		o = o.substring(0, o.length - 1);
		var other = o.split(",");
		var unique = uniqueArray(other);
		var str = replaceString(unique, elmnt.attr("id") + ",", "");
		unique = str;
		unique = unique.substring(0, unique.length - 1);
		var others = unique.split(",");

		//get cos and sin of angle for projection on new axis
		var v1 = getVector(tl, bl);
		var v2 = getVector(bl, br);

		//if other elements exist in same segments as target element
		if(others[0].length) {
			for (k = 0; k < others.length; k++) {
				var second = $("#"+others[k]);
				var otl = second.find(".tl");
				var otr = second.find(".tr");
				var obr = second.find(".br");
				var obl = second.find(".bl");
				var v3 = getVector(otl, obl);
				var v4 = getVector(obl, obr);
				var e_points =[];
				var o_points = [];
				//e_points contains projection of each corner onto each of the four axis for target element
				//o_points contains projection of each corner onto each of the four axis for other element
				e_points[0] = getAllCoordinates(tl, bl, br, tr, v1);
				e_points[1] = getAllCoordinates(tl, bl, br, tr, v2);
				e_points[2] = getAllCoordinates(tl, bl, br, tr, v3);
				e_points[3] = getAllCoordinates(tl, bl, br, tr, v4);
				o_points[0] = getAllCoordinates(otl, obl, obr, otr, v1);
				o_points[1] = getAllCoordinates(otl, obl, obr, otr, v2);
				o_points[2] = getAllCoordinates(otl, obl, obr, otr, v3);
				o_points[3] = getAllCoordinates(otl, obl, obr, otr, v4);
				var c = 0;

				//if projections for both elements intersect on all four axis, it is collision, otherwise it is not collision
				for (j=0; j<4; j++) {
					var x1 = Math.min.apply(null, e_points[j]);
					var x2 = Math.max.apply(null, e_points[j]);
					var y1 = Math.min.apply(null, o_points[j]);
					var y2 = Math.max.apply(null, o_points[j]);
					if(Math.max(x1,y1) <= Math.min(x2,y2)) {
						c++;
					}
				}
				if(c == 4) {
					return true;
				}
			}
		}
		return false;
	}

	//find unique elements of array and put into string
	function uniqueArray(array) {
		var unique = [];
		var str = "";
		for(i = 0; i < array.length; i++) {
			if (unique.indexOf(array[i])===-1) {
				unique.push(array[i]); 
				str = str + array[i] + ",";
			}
		}
		return str;
	}

	//getting sin and cos for rotated angle
	function getVector(p1, p2) {
		var v = [];
		var a = p1.offset().left - p2.offset().left;
		var b = p1.offset().top - p2.offset().top;
		v[0] = a/Math.sqrt(Math.pow(a,2)+Math.pow(b,2));
		v[1] = b/Math.sqrt(Math.pow(a,2)+Math.pow(b,2));
		return v;
	}
	
	//get all projection coordinates for one element
	function getAllCoordinates(p1, p2, p3, p4, v) {
		var p_new = [];
		p_new[0] = getCoordinates(v, p1);
		p_new[1] = getCoordinates(v, p2);
		p_new[2] = getCoordinates(v, p3);
		p_new[3] = getCoordinates(v, p4);
		return p_new;
	}

	//get coordinate for one corner
	function getCoordinates(v, p) {
		return p_proj = p.offset().left * v[0] + p.offset().top * v[1];
	}

	
	//Detect if element goes over edge of the room
	function detectOverEdge(elmnt) {
		var t1 = elmnt.offset().top + elmnt.outerHeight();
		var l1 = elmnt.offset().left + elmnt.outerWidth();
		var t2 = drop.offset().top + drop.outerHeight() - borderDrop;
		var l2 = drop.offset().left + drop.outerWidth() - borderDrop;
        
		if (elmnt.offset().left < (drop.offset().left + borderDrop) || elmnt.offset().top < (drop.offset().top + borderDrop) || t1 > t2 || l1 > l2) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
		
	
	//Dragg element
	function draggElement(elmnt) {
		elmnt.draggable({
			// update elmntBelongs; if collision exists, revert to previous position
			revert: function(){
				elementSegments(elmnt, segments, elmntBelongs);
				if(detColl(elmnt)){
				   return true;
				}
				else {
					return false;
				}
			},
			// update elmntBelongs if elmnt is reverted
			stop: function(){
				if(detColl(elmnt)){
					elementSegments(elmnt, segments, elmntBelongs);
				}
			},
			containment: elmnt.parent(),
			cancel: '.rotate_icon, .remove_icon, .text_input'
		});	
	}
	
	
	//Rotate
	//rotButton - rotatable icon
	//elmnt - draggable div
	//rotDiv - rotatable div
	//drop - #droppable div
	//oldParam are parameters for previous state
	function rotatingAngle(elmnt, rotButton, rotDiv, drop, oldParam){
		var rotating = false;
		var prevPos = false;
		
		//On mousedown rotating starts
		rotButton.mousedown(function() {	
			rotating = true;
			prevPos = true;				
		});
		
		//On mouse up rotating stops
		$(document).mouseup(function () {
				rotating = false;
				
				//Check if element goes outside #droppable or overlaps with other elements
				if (prevPos) {	
					var over = false;
					if(detectOverEdge(elmnt)) {
						over = true;
					}
					else {
						if(detColl(elmnt)) {
							over = true;
						}
					}					
				
				
					//If elemnt falls out or overlaps, set its parameters to previous position
					if(over) {
						elmnt.width(oldParam['oldeW']).height(oldParam['oldeH']);				
						elmnt.css({
							left: oldParam['oldeLeft'],
							top: oldParam['oldeTop']
						});
						rotDiv.width(oldParam['oldW']).height(oldParam['oldH']);	
						rotDiv.css({
							left: oldParam['oldLeft'],
							top: oldParam['oldTop'],
							transform: 'rotate(' + oldParam['oldD'] + 'deg)'
						});	
						elementSegments(elmnt, segments, elmntBelongs);
					}
				prevPos = false;	
				}
		});
		
		//While the element is being rotated
		//On mousemove, if rotating=true (mousedown) rotate 
		$(document).mousemove(function (event, ui) {	
			var cent  = {
				x: elmnt.offset().left + elmnt.width()/2, 
				y: elmnt.offset().top + elmnt.height()/2
			}; 
			var radians = Math.atan2(event.pageY - cent.y, event.pageX - cent.x);
			var degree = radians * (180 / Math.PI); 
			
			if (rotating) {	 
				var w = rotDiv.width() + 2*borderElmnt;
				var h = rotDiv.height() + 2*borderElmnt;
				var h_new = h * Math.abs(Math.cos(radians)) + w * Math.abs(Math.sin(radians));
				var w_new = w * Math.abs(Math.cos(radians)) + h * Math.abs(Math.sin(radians));		
				
				//rotate #rotatable div for calculated degree
				rotDiv.css({
					transform: 'rotate(' + degree + 'deg)'		 
				});
				
				//change elemnt width and height to match #rotatable div dimensions
				elmnt.width(Math.round(w_new)).height(Math.round(h_new));
				
				//find size and position of element relative to viewport
				var rotRect = rotDiv[0].getBoundingClientRect();

				//set #draggable to same position as #rotatble
				elmnt.offset({
					left: Math.round(rotRect.left),
					top: Math.round(rotRect.top)
				});
				//center #rotatble inside #draggable
				rotDiv.css({
					left: Math.round((w_new-w)/2),
					top: Math.round((h_new-h)/2)
				});

				//update segments
				elementSegments(elmnt, segments, elmntBelongs);
			}				
		});
	}
	
	
	//Resize
	//elmnt - element to be resized
	//rot - subdiv to also be resized
	//eW, eH, eF - width, height and font-size of element at the beginning of the process
	function resizeElement(elmnt, rotDiv, eW, eH, eF) {
		rotDiv.resizable({ 
			minWidth: 50,
			minHeight: 50,
			resize: function(event, ui){
				//width and height of #rotatble div
				var rw = parseInt(rotDiv.css("width"));
				var rh = parseInt(rotDiv.css("height"));
				//if there is a text input, scale its font
				if (rotDiv.hasClass("rotatable_shelf")){
					//calculate width and height rate and scale text font according to smaller rate
					var wRate = rotDiv.outerWidth() / eW;
					var hRate = rotDiv.outerHeight() / eH;	
					var scale = (hRate > wRate) ? wRate : hRate;				
					var newFont = Math.round(2 * eF * scale);
					var textF = rotDiv.find(".text_input");
					
					textF.css({
						"font-size": newFont
					});						
					
					//resize and position rotate and remove icons for shelf
					var r = (rh > rw) ? rw : rh;				
					
					rotDiv.find(".rotate_shelf").css({
						width: Math.round(0.2*r),
						height: Math.round(0.2*r),
						left: rw-Math.round(0.2*r)
					});
					rotDiv.find(".remove_shelf").css({
						width: Math.round(0.2*r),
						height: Math.round(0.2*r),
						top: rh-2*Math.round(0.2*r)
					});
				
				}
				//resize and position rotate and remove icons for wall
				else {
					var r1 = (rh > 0.2*rw) ? 0.2*rw : rh;
					rotDiv.find(".rotate_wall").css({
						width: Math.round(r1),
						height: Math.round(r1),
						left: Math.round((rw-r1)/2)
					});
					rotDiv.find(".remove_wall").css({
						width: Math.round(r1),
						height: Math.round(r1),
						top: 0
					});
				}
			},
			stop: function(event, ui){	
				var rw = parseInt(rotDiv.css("width"));
				var rh = parseInt(rotDiv.css("height"));
				
				var rotRect = rotDiv[0].getBoundingClientRect();
				
				var w = elmnt.width();
				var h = elmnt.height();
				var w_new = rotRect.width;
				var h_new = rotRect.height;
				
				elmnt.width(Math.round(w_new)).height(Math.round(h_new));
				elmnt.offset({
					left: Math.round(rotRect.left),
					top: Math.round(rotRect.top)
				});
				rotDiv.offset({
					left: Math.round(rotRect.left),
					top: Math.round(rotRect.top)
				});
				
				if (rotDiv.hasClass("rotatable_shelf")){
					//calculate width and height rate and scale text font according to smaller rate
					var wRate = rotDiv.outerWidth() / eW;
					var hRate = rotDiv.outerHeight() / eH;	
					var scale = (hRate > wRate) ? wRate : hRate;				
					var newFont = Math.round(2 * eF * scale);
					var textF = rotDiv.find(".text_input");
					
					textF.css({
						"font-size": newFont
					});				
					
					var r = (rh > rw) ? rw : rh;				
					
					rotDiv.find(".rotate_shelf").css({
						width: Math.round(0.2*r),
						height: Math.round(0.2*r),
						left: rw-Math.round(0.2*r)
					});
					rotDiv.find(".remove_shelf").css({
						width: Math.round(0.2*r),
						height: Math.round(0.2*r),
						top: rh-2*Math.round(0.2*r)
					});
				
				}
				else {
					var r1 = (rh > 0.2*rw) ? 0.2*rw : rh;
					rotDiv.find(".rotate_wall").css({
						width: Math.round(r1),
						height: Math.round(r1),
						left: Math.round((rw-r1)/2)
					});
					rotDiv.find(".remove_wall").css({
						width: Math.round(r1),
						height: Math.round(r1),
						top: 0
					});
				}
				elementSegments(elmnt, segments, elmntBelongs);
				
				var rot_icon = rotDiv.find(".rotate_icon");
				var rem_icon = rotDiv.find(".remove_icon");

				
				var over = false;
					if(detectOverEdge(elmnt)) {
						over = true;
					}
					else {
						if(detColl(elmnt)) {
							over = true;
						}
					}	
				
				//if element is in collision go back to previous size 
					if(over) {
						
						rotDiv.css({
							width: parseInt(ui.originalSize.width),
							height: parseInt(ui.originalSize.height)
						});	
						
						var rotRect = rotDiv[0].getBoundingClientRect();
				
						var w = elmnt.width();
						var h = elmnt.height();
						var w_new = rotRect.width;
						var h_new = rotRect.height;
						
						elmnt.width(Math.round(w_new)).height(Math.round(h_new));
						elmnt.offset(rotDiv.offset());
						rotDiv.offset({
							left: Math.round(rotRect.left),
							top: Math.round(rotRect.top)
						});
												
												
						//calculate image size according to smaller width or height
						
						if (rotDiv.hasClass("rotatable_shelf")){
							var wRate = rotDiv.outerWidth() / eW;
							var hRate = rotDiv.outerHeight() / eH;	
							var scale = (hRate > wRate) ? wRate : hRate;
							var newFont = Math.round(2 * eF * scale);
							textF.css({
								"font-size": newFont
							});	

							var rot_size = (parseInt(rotDiv.css("height")) > parseInt(rotDiv.css("width"))) ? Math.round(0.2*parseInt(rotDiv.css("width"))) : Math.round(0.2*parseInt(rotDiv.css("height")));
							rot_icon.css({
								width: Math.round(rot_size),
								height: Math.round(rot_size)
							});
							rem_icon.css({
								width: Math.round(rot_size),
								height: Math.round(rot_size)
							});
							rot_icon.css({
								left: parseInt(rotDiv.css("width")) - parseInt(rot_icon.css("width")),
								top: 0
							});
							rem_icon.css({
								left: 0,
								top: parseInt(rotDiv.css("height")) - parseInt(rem_icon.css("height")) - parseInt(rot_icon.css("height"))
							});
						}
						else {
							var rot_size = (ui.originalSize.height > 0.2 * ui.originalSize.width ? Math.round(0.2 * ui.originalSize.width) : ui.originalSize.height);
							rot_icon.css({
								width: Math.round(rot_size),
								height: Math.round(rot_size)
							});
							rem_icon.css({
								width: Math.round(rot_size),
								height: Math.round(rot_size)
							});
							rot_icon.css({
								left: Math.round(0.5 * (parseInt(rotDiv.css("width")) - parseInt(rot_icon.css("width")))),
								top: 0
							});
							rem_icon.css({
								left:0,
								top: 0
							});
						}
						
						elementSegments(elmnt, segments, elmntBelongs);
					}		
			},
			containment: elmnt.parent()
        });	
	}
	
	
 //Calculate angle from transformation
	function calculateAngle(elmnt) {
		var transform = elmnt.css("transform");
		if (transform == "none"){
			var angle = 0;
		}
		else {
			var parts = transform.substring(7,transform.length-1);
			parts = parts.split(',');
			var a = parts[0];
			var b = parts[1];
			var radians = Math.atan2(b, a);
			angle = radians * (180/Math.PI);	
		}	
		return angle;		
	}

	// Clone and drag element
	$( ".wrapper" ).draggable({
		helper: "clone"
	});
	// Set initial counter numbers
	var counter_shelf = $(".wrapper_shelf").length;
	var counter_wall = $(".wrapper_wall").length;
	
	//Droppable area
	drop.droppable({
		tolerance: "fit",
		drop: function (event, ui) {
			if(ui.draggable.hasClass("wrapper_shelf") || ui.draggable.hasClass("wrapper_wall")) {			
				// Clone and append element 
				var elmnt = $(ui.helper).clone();
				$(this).append(elmnt);
				// Element position is absolute to its parent (droppable area) 
				// When drraged, element position is absolute to viewport
				// Update element position so it stays where it is dropped
				var elTop = elmnt.position().top - drop.offset().top - borderDrop;
				var elLeft = elmnt.position().left - drop.offset().left - borderDrop;
				elmnt.css({
					top: elTop,
					left: elLeft
				});

				// Set initial parameters
				var initArray = initClone(elmnt, counter_shelf, counter_wall);

				// If element is not in collision with any other element set additional parameters when rotation starts
				if(!removeCollision(elmnt, elmntBelongs)) {
				var array = [];
				initArray['rotIcon'].mousedown(function() {	
					array['oldeW'] = elmnt.width();
					array['oldeH'] = elmnt.height();
					array['oldeLeft'] = elmnt.css("left");
					array['oldeTop'] = elmnt.css("top");
					array['oldW'] = initArray['rot'].width();
					array['oldH'] = initArray['rot'].height();
					array['oldLeft'] = initArray['rot'].css("left");
					array['oldTop'] = initArray['rot'].css("top");
					array['oldD'] = calculateAngle(initArray['rot']);
				});				
				initArray['rotIcon'].trigger("mousedown");	
				
				
				var eH = elmnt.outerHeight();
				var eW = elmnt.outerWidth();
				var eF = parseInt(elmnt.css("font-size"));

				//Draggable
				draggElement(elmnt);
								
				//Resizable
				resizeElement(elmnt, initArray['rot'], eW, eH, eF);
			
				//Rotatable
				rotatingAngle(elmnt, initArray['rotIcon'], initArray['rot'], drop, array);		

				//Removable					
				initArray['remIcon'].click(function () {
					removeElement(elmnt);
				});
				
				counter_shelf = initArray['counter_shelf'];
				counter_wall = initArray['counter_wall'];
				}
			}
		}
	});
 });